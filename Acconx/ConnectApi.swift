//
//  ConnectApi.swift
//  E-Auction
//
//  Created by Tana Chaijamorn on 12/27/2558 BE.
//  Copyright © 2558 Tana Chaijamorn. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

protocol ConnectApiDelegate
{
    func getReceiveData(data:AnyObject,name:String)
}

class ConnectApi: NSObject,NSURLConnectionDelegate,NSURLConnectionDataDelegate,NSXMLParserDelegate {

    var responseData: NSMutableData!
    var nameApi: String!
    var delegate: ConnectApiDelegate?
    var currentElementValue: NSMutableString!
    
    func ApiLogin(username:NSString,password:NSString) {
        nameApi = "Login"
        
        let headers = [
            "content-type": "application/x-www-form-urlencoded"
        ]
        
        let postData = NSMutableData(data: "username=\(username)".dataUsingEncoding(NSUTF8StringEncoding)!)
        postData.appendData("&password=\(password)".dataUsingEncoding(NSUTF8StringEncoding)!)
        postData.appendData("&grant_type=password".dataUsingEncoding(NSUTF8StringEncoding)!)
        
        let request = NSMutableURLRequest(URL: NSURL(string: "https://api.kaojao.com/token")!,
                                          cachePolicy: .UseProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.HTTPMethod = "POST"
        request.allHTTPHeaderFields = headers
        request.HTTPBody = postData
        
        
        let session = NSURLSession.sharedSession()
        let dataTask = session.dataTaskWithRequest(request, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                print(error)
                
                self.delegate?.getReceiveData(404, name: self.nameApi)
                
            } else {
                let httpResponse = response as? NSHTTPURLResponse
                
                do {
                    let json = JSON(data: data!);
                    if json["token_type"].string != nil {
                        let tokentype : String = json["token_type"].string!
                        if json["access_token"].string != nil {
                            let access_token : String = json["access_token"].string!
                            let User : String = tokentype + " " + access_token
                            
                            NSUserDefaults.standardUserDefaults().setObject(User, forKey: "UserToken")
                            NSUserDefaults.standardUserDefaults().setObject(username, forKey: "username")
                            NSUserDefaults.standardUserDefaults().setObject(password, forKey: "password")
                        }
                    }
                } catch {
                    print("Error: \(error)")
                }
                
                self.delegate?.getReceiveData(httpResponse!.statusCode, name: self.nameApi)
            }
        })
        
        dataTask.resume()
        
    }
    
    func APIloadData(method:String, user:String, page:Int,
                     completionHandler: (NSDictionary?, NSError?) -> ()) {
        if method == "Income" {
            let headers = [
                "Authorization": user,
                "Accept": "application/json"
            ]
            let url :String = String.init(format: "https://api.kaojao.com/api/incomes/list?pageIndex=%d&pageSize=10&sortBy=incomeDate", page)
            Alamofire.request(.GET, url, headers: headers)
                .responseJSON { response in
                    switch response.result {
                    case .Success(let JSON):
                        completionHandler(JSON as? NSDictionary, nil)
                    case .Failure(let error):
                        completionHandler(nil, error)
                    }
            }
        }else if method == "Estimate" {
            let headers = [
                "Authorization": user,
                "Accept": "application/json"
            ]
            let url :String = String.init(format: "https://api.kaojao.com/api/estimates/list?pageIndex=%d&pageSize=10&sortBy=estimateDate", page)
            Alamofire.request(.GET, url, headers: headers)
                .responseJSON { response in
                    switch response.result {
                    case .Success(let JSON):
                        completionHandler(JSON as? NSDictionary, nil)
                    case .Failure(let error):
                        completionHandler(nil, error)
                    }
            }
        }else if method == "Expenses" {
            let headers = [
                "Authorization": user,
                "Accept": "application/json"
            ]
            let url :String = String.init(format: "https://api.kaojao.com/api/expenses/list?pageIndex=%d&pageSize=10&sortBy=expenseDate", page)
            Alamofire.request(.GET, url, headers: headers)
                .responseJSON { response in
                    switch response.result {
                    case .Success(let JSON):
                        completionHandler(JSON as? NSDictionary, nil)
                    case .Failure(let error):
                        completionHandler(nil, error)
                    }
            }
        }else if method == "Clients" {
            let headers = [
                "Authorization": user,
                "Accept": "application/json"
            ]
            let url :String = String.init(format: "https://api.kaojao.com/api/clients/list?pageIndex=%d&pageSize=10&reverse=true&sortBy=createdDate", page)
            Alamofire.request(.GET, url, headers: headers)
                .responseJSON { response in
                    switch response.result {
                    case .Success(let JSON):
                        completionHandler(JSON as? NSDictionary, nil)
                    case .Failure(let error):
                        completionHandler(nil, error)
                    }
            }
        }else if method == "Items" {
            let headers = [
                "Authorization": user,
                "Accept": "application/json"
            ]
            let url :String = String.init(format: "https://api.kaojao.com/api/items/list?pageIndex=%d&pageSize=10&reverse=true&sortBy=createdDate", page)
            Alamofire.request(.GET, url, headers: headers)
                .responseJSON { response in
                    switch response.result {
                    case .Success(let JSON):
                        completionHandler(JSON as? NSDictionary, nil)
                    case .Failure(let error):
                        completionHandler(nil, error)
                    }
            }
        }
        
        
    }
    
    func checkAPI(token:String, completionHandler: (NSDictionary?, NSError?) -> ())  {
        let headers = [
            "Authorization": token,
            "Accept": "application/json"
        ]
        let url :String = String.init(format: "https://api.kaojao.com/api/incomes/list?pageIndex=1&pageSize=10&sortBy=incomeDate")
        Alamofire.request(.GET, url, headers: headers)
            .responseJSON { response in
                switch response.result {
                    
                case .Success(let JSON):
                    completionHandler(JSON as? NSDictionary,nil)
                case .Failure(let error):
                    completionHandler(nil, error)
                }
        }
        
    }
    
    func extranalLogin(Provider:String,AccessToken:String,email:String, completionHandler: (NSDictionary?, NSError?) -> ())  {
        
        let headers = [
            "Accept": "application/json"
        ]
        let parameter = [
            "provider": Provider,
            "email": email,
            "externalAccessToken": AccessToken
        ]
        
        let url :String = String.init(format: "https://api.kaojao.com/api/User/ExternalLogin")
        Alamofire.request(.POST, url, parameters: parameter, headers: headers)
//        Alamofire.request(.POST, url, headers: headers)
            .responseJSON { response in
                switch response.result {
                    
                case .Success(let JSON):
                    completionHandler(JSON as? NSDictionary,nil)
                case .Failure(let error):
                    completionHandler(nil, error)
                }
        }
        
    }
    
    func createItem(user:String, nameString:String, sellInt:NSInteger,
                    completionHandler: (NSDictionary?, NSError?) -> ())  {
        let headers = [
            "Authorization": user,
            "Accept": "application/json"
        ]
        let parameter = [
            "code": "",
            "id": 0,
            "description": "",
            "name": nameString,
            "quantity": 1,
            "sellPrice": sellInt,
            "trackInventory": "false"
        ]
        let url :String = String.init(format: "https://api.kaojao.com/api/items/create")
        
        Alamofire.request(.POST, url, parameters: parameter as? [String : AnyObject], headers: headers)
            //        Alamofire.request(.POST, url, headers: headers)
            .responseJSON { response in
                switch response.result {
                    
                case .Success(let JSON):
                    completionHandler(JSON as? NSDictionary,nil)
//                    print(JSON)
                    
                case .Failure(let error):
                    completionHandler(nil, error)
                }
        }
    }
    
    func createLinkforUploadImage(IDItem: String,ItemCount: Int,
        completionHandler: (NSDictionary?, NSError?) -> ()) {
        let token = NSUserDefaults.standardUserDefaults().valueForKey("UserToken") as? String
        
        let itemCountString = String(ItemCount)
        
        let url :String = String.init(format: "https://api.kaojao.com/api/storage/getwritesas?blobName=items/"+IDItem+"/"+itemCountString+".png")
        
        Alamofire.request(
            .GET,
            url,
            parameters: nil,
            headers: ["Authorization": token!]
            )
            .responseJSON { response in
                switch response.result {
                    
                case .Success(let JSON):
                    completionHandler(JSON as? NSDictionary,nil)
                    
                case .Failure(let error):
                    completionHandler(nil, error)
                }
        }
    }
    
    func uploadImagetoApi(urlItem: String,imageData: NSData,
                          completionHandler: (NSDictionary?, NSError?) -> ()) {
        Alamofire.upload(.PUT, urlItem,headers: ["x-ms-blob-type": "BlockBlob"],data: imageData)
//            .response { request, response, data, error in
//               print(request)
//               print(response)
//               print(data)
//               print(error)
//            }
            .responseJSON { response in
                switch response.result {
                    
                case .Success(let JSON):
                    completionHandler(JSON as? NSDictionary,nil)
                    print(JSON)
                    
                case .Failure(let error):
                    completionHandler(nil, error)
                    print(error)
                }
        }
    }
}
