//
//  ViewController.swift
//  Acconx
//
//  Created by Tana Chaijamorn on 3/29/2559 BE.
//  Copyright © 2559 Tana Chaijamorn. All rights reserved.
//

import UIKit
import SWRevealViewController

class HomeViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    var menulist = []
    
    @IBOutlet weak var tablemenu: UITableView!
    @IBOutlet weak var AddItemBtn: UIButton!
    @IBOutlet var menuButton:UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.setNavigationbarToTranspalent()
        self.navigationItem.setRightBarButtonItem(menuButton, animated: true)
        self.navigationItem.setHidesBackButton(true, animated:true);
        if self.revealViewController() != nil {
            menuButton.target = self.revealViewController()
            menuButton.action = "rightRevealToggle:"
//            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            self.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
            self.AddItemBtn.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
            self.tablemenu.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        }
        self.menulist = [NSLocalizedString("incomeTypeIncome", comment: "incomeTypeIncome"),
                         NSLocalizedString("estimateTypeEstimate", comment: "estimateTypeEstimate"),
                         NSLocalizedString("accCategoryExpenses", comment: "accCategoryExpenses"),
                         NSLocalizedString("clients", comment: "clients"),
                         NSLocalizedString("items", comment: "items")]
        
        self.tablemenu.reloadData()
    }

    // MARK: - Set UI
    func setNavigationbarToTranspalent() {
        self.navigationController?.navigationBarHidden = false
        
        let bar:UINavigationBar! =  self.navigationController?.navigationBar
        
        if (bar == nil) {
            
        }else{
            bar.setBackgroundImage(UIImage(), forBarMetrics: UIBarMetrics.Default)
            bar.shadowImage = UIImage()
            bar.backgroundColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.0)
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.menulist.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("menulist", forIndexPath: indexPath) as! MenuTableViewCell
        cell.backgroundColor = UIColor.clearColor()
        cell.selectionStyle = .None
        cell.TitleLabel?.text = "\(self.menulist[indexPath.row])"
        if self.menulist[indexPath.row] as! String == NSLocalizedString("incomeTypeIncome", comment: "incomeTypeIncome") {
            cell.Iconimg.image = UIImage.init(named: "income")
        }else if self.menulist[indexPath.row] as! String == NSLocalizedString("estimateTypeEstimate", comment: "estimateTypeEstimate") {
            cell.Iconimg.image = UIImage.init(named: "estimate")
        }else if self.menulist[indexPath.row] as! String == NSLocalizedString("accCategoryExpenses", comment: "accCategoryExpenses") {
            cell.Iconimg.image = UIImage.init(named: "expenses")
        }else if self.menulist[indexPath.row] as! String == NSLocalizedString("clients", comment: "clients") {
            cell.Iconimg.image = UIImage.init(named: "clients")
        }else if self.menulist[indexPath.row] as! String == NSLocalizedString("items", comment: "items") {
            cell.Iconimg.image = UIImage.init(named: "items")
        }
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.row == 0 {
            let DetailVC = self.storyboard?.instantiateViewControllerWithIdentifier("DetailIncomeView") as? DetailIncomeViewController
            DetailVC?.typelist = "Income"
            self.navigationController?.pushViewController(DetailVC!, animated: true)
        }else if indexPath.row == 1{
            let DetailVC = self.storyboard?.instantiateViewControllerWithIdentifier("DetailIncomeView") as? DetailIncomeViewController
            DetailVC?.typelist = "Estimate"
            self.navigationController?.pushViewController(DetailVC!, animated: true)
        }else if indexPath.row == 2{
            let DetailVC = self.storyboard?.instantiateViewControllerWithIdentifier("DetailIncomeView") as? DetailIncomeViewController
            DetailVC?.typelist = "Expenses"
            self.navigationController?.pushViewController(DetailVC!, animated: true)
        }else if indexPath.row == 3{
            let ClintsVC = self.storyboard?.instantiateViewControllerWithIdentifier("ClientView") as? ClientViewController
            self.navigationController?.pushViewController(ClintsVC!, animated: true)
        }else if indexPath.row == 4{
            let DetailVC = self.storyboard?.instantiateViewControllerWithIdentifier("DetailIncomeView") as? DetailIncomeViewController
            DetailVC?.typelist = "Items"
            self.navigationController?.pushViewController(DetailVC!, animated: true)
        }
        
    }
    
    @IBAction func passonAddNew(sender: AnyObject) {
        let AddVC = self.storyboard?.instantiateViewControllerWithIdentifier("AddDataView") as? AddDataViewController
        self.navigationController?.pushViewController(AddVC!, animated: true)
    }
    
    func poptohome() {
        
    }
}

