//
//  ClientViewController.swift
//  Acconx
//
//  Created by Tana Chaijamorn on 5/24/2559 BE.
//  Copyright © 2559 Tana Chaijamorn. All rights reserved.
//

import UIKit
import CCBottomRefreshControl
import MBProgressHUD
import CGLAlphabetizer

class ClientViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    var dataList : NSMutableArray = []
    var dic : NSDictionary = [:]
    var titleIndex: NSArray = []
    var refreshControl: UIRefreshControl!
    let api = ConnectApi()
    var pages : Int = 1
    let user : String = (NSUserDefaults.standardUserDefaults().valueForKey("UserToken") as? String)!
    @IBOutlet var menuButton:UIBarButtonItem!
    @IBOutlet weak var tableClient: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = NSLocalizedString("clients", comment: "clients")
//        var resultIndex: NSMutableArray = []
        // Do any additional setup after loading the view.
        dataList = NSMutableArray()
        let hud = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        hud.mode = MBProgressHUDMode.Indeterminate
        hud.labelText = "Loading"
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
            self.api.APIloadData("Clients", user: self.user, page: self.pages, completionHandler: { responseObject, error in
//                print("\(responseObject) \n  \(error) ")
                
                self.dataList.addObjectsFromArray((responseObject?.objectForKey("collection") as? NSArray)! as [AnyObject])
                
                self.dic = NSDictionary(dictionary: CGLAlphabetizer.alphabetizedDictionaryFromObjects(self.dataList as [AnyObject], usingKeyPath: "contactName"))
                
                self.titleIndex = Array(CGLAlphabetizer.indexTitlesFromAlphabetizedDictionary(self.dic as [NSObject : AnyObject]))
                hud.hide(true)
                self.tableClient.reloadData()
            })
        }
        
        
        
        self.navigationItem.setRightBarButtonItem(menuButton, animated: true)
        //        self.navigationItem.setHidesBackButton(true, animated:true);
        
        
        // Do any additional setup after loading the view.
        self.tableClient.estimatedRowHeight = 68
        self.tableClient.rowHeight = UITableViewAutomaticDimension
        self.tableClient.contentInset = UIEdgeInsetsMake(-64, 0, 0, 0)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return titleIndex.count
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let title : String = titleIndex[section] as! String
        return dic[title]!.count
    }
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return titleIndex[section] as? String
    }
    func sectionIndexTitlesForTableView(tableView: UITableView) -> [String]?{
       return titleIndex as? [String]
    
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("ClientAndItemsCell", forIndexPath: indexPath) as! ClientAndItemsCell
        if (indexPath.row % 2) != 0 {
            cell.backgroundColor = UIColor.init(hex: "F7F7F7")
        }else{
            cell.backgroundColor = UIColor.whiteColor()
        }
        
        cell.selectionStyle = .None
        cell.priceLabel.text = ""
        
        let client : String = self.ACobjectAtIndexPath(indexPath)
        cell.itemLabel.text = dic[client]![indexPath.row]["contactName"] as? String
        
        return cell
        
    }

    func ACobjectAtIndexPath(indexPath: NSIndexPath) -> String {
        return (titleIndex[indexPath.section] as? String)!
    }
}
