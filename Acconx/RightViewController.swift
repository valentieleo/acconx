//
//  RightViewController.swift
//  Acconx
//
//  Created by Tana Chaijamorn on 4/26/2559 BE.
//  Copyright © 2559 Tana Chaijamorn. All rights reserved.
//

import UIKit
import SWRevealViewController

class RightViewController: UIViewController {

    @IBOutlet weak var LogoutBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func LogOutFunc(sender: AnyObject) {
        NSUserDefaults.standardUserDefaults().removeObjectForKey("UserToken")
        
//        navigationController!.popToViewController(navigationController!.viewControllers[0], animated: false)
        
//        let SWReveal = SWRevealViewController()
//        let login = self.storyboard?.instantiateViewControllerWithIdentifier("LoginView") as? LoginViewController
//        
//        let navigation = UINavigationController.init(rootViewController: login!)
//        SWReveal.pushFrontViewController(navigation, animated: false)
        
    }
    
}
