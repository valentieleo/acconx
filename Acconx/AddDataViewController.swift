//
//  AddDataViewController.swift
//  Acconx
//
//  Created by Tana Chaijamorn on 7/24/2559 BE.
//  Copyright © 2559 Tana Chaijamorn. All rights reserved.
//

import UIKit
import MBProgressHUD
import DKImagePickerController

class AddDataViewController: UIViewController, UITextFieldDelegate, UICollectionViewDelegate,UICollectionViewDataSource,ConnectApiDelegate {

    @IBOutlet var textView : UIPlaceHolderTextView?
    @IBOutlet weak var savebtn: UIButton!
    @IBOutlet weak var nameItem: UITextView!
    @IBOutlet weak var currencyLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var valuePrice: UITextField!
    @IBOutlet weak var widthLabel: NSLayoutConstraint!
    @IBOutlet weak var cancelbtn: UIButton!
    @IBOutlet weak var imageCollection: UICollectionView!
    
    var assets: [DKAsset]?
    var dataList : NSMutableArray = []
    
    let currencyTitle = NSUserDefaults.standardUserDefaults().valueForKey("Currency") as? String
    private let reuseIdentifier = "ACImageCell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNavigationbarToTranspalent()
        // Do any additional setup after loading the view.
        self.title = NSLocalizedString("newItem", comment: "newItem")
        textView?.placeholder = "Product Name"
        currencyLabel.text = currencyTitle
        priceLabel.text = NSLocalizedString("price", comment: "price")
        savebtn.setTitle(NSLocalizedString("save", comment: "save"), forState: UIControlState.Normal)
        cancelbtn.setTitle(NSLocalizedString("cancel", comment: "cancel"), forState: UIControlState.Normal)
        valuePrice.placeholder = "0.00"
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        UINavigationBar.appearance().tintColor = UIColor.whiteColor()
        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName : UIColor.whiteColor()]
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Set UI
    func setNavigationbarToTranspalent() {
        self.navigationController?.navigationBarHidden = false
        
        let bar:UINavigationBar! =  self.navigationController?.navigationBar
        
        if (bar == nil) {
            
        }else{
            bar.setBackgroundImage(UIImage(), forBarMetrics: UIBarMetrics.Default)
            bar.shadowImage = UIImage()
            bar.backgroundColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.0)
        }
        
    }
    
    @IBAction func passOnSaveBtn(sender: AnyObject) {
        self.connectAPI()
    }
    
    @IBAction func passOnCancelBtn(sender: AnyObject) {
        self.navigationController!.popViewControllerAnimated(true)
    }

    @IBAction func ChangeText(sender: AnyObject) {
        widthLabel.constant = valuePrice.intrinsicContentSize().width
        valuePrice.layoutIfNeeded()
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        self.valuePrice.resignFirstResponder()
        return true
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        let count = self.assets?.count ?? 0
        
        if count <= 3 {
            return count + 1
        }else{
            return count
        }
        
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(reuseIdentifier, forIndexPath: indexPath) as! ACImageCollectionViewCell
        
        
        if indexPath.row + 1 <= self.assets?.count ?? 0 {
            cell.backgroundColor = UIColor.clearColor()
            cell.textLabel.hidden = true
            cell.imageData.hidden = false
            let asset = self.assets![indexPath.row]
            
            let layout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
            
            asset.fetchImageWithSize(layout.itemSize.toPixel(), completeBlock: { image, info in
                cell.imageData.image = image
                
            })
            
        } else {
            cell.backgroundColor = UIColor.init(hex: "F2F2F2")
            cell.textLabel.text = "+ Photos"
            cell.textLabel.hidden = false
            cell.imageData.hidden = true
        }
        
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        
        self.callImagePicker()
        
    }
    
    func collectionView(collectionView : UICollectionView,layout collectionViewLayout:UICollectionViewLayout,sizeForItemAtIndexPath indexPath:NSIndexPath) -> CGSize
    {
        let cellSize:CGSize = CGSizeMake(self.imageCollection.frame.width/2-10, self.imageCollection.frame.width/2)
        return cellSize
    }
    
    func getReceiveData(data: AnyObject, name: String) {
        dispatch_async(dispatch_get_main_queue(), {
            if data as! Int == 200 {
                MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
            }else{
                MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
            }
        })
        
    }
    
    func connectAPI(){
        let api = ConnectApi()
        api.delegate = self
        var token = NSUserDefaults.standardUserDefaults().valueForKey("UserToken") as? String
        
        if (token == nil) {
            token = ""
        }
        let name = textView!.text!
        var price = valuePrice!.text!
        if price == "" {
            price = String(0)
        }
        let hud = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        
        api.createItem(token!, nameString: name, sellInt: Int(price)!, completionHandler: { responseObject, error in
            
            let idPic = responseObject!.objectForKey("id") as? NSNumber
            let idPicString = String(idPic!)
            
            for i in 0 ..< self.assets!.count {
                let asset = self.assets![i]
                
                api.createLinkforUploadImage(idPicString, ItemCount: i+1, completionHandler: { responseObject, error in
                    asset.fetchOriginalImage(true, completeBlock: { (image, info) in
                        
                        let imageData: NSData = UIImageJPEGRepresentation(image!, 90)!
                        api.uploadImagetoApi(responseObject?.valueForKey("uri") as! String, imageData: imageData, completionHandler: { responsefromapi, error in
                            
                            if i == self.assets!.count-1 {
                                hud.hide(true)
                                self.navigationController!.popViewControllerAnimated(true)
                            }
                        })
                        
                    })
                    
                })
            }
            
            
        })
    }
    
    func callImagePicker() {
        let pickerController = DKImagePickerController()
        
        pickerController.defaultSelectedAssets = self.assets
        
        pickerController.maxSelectableCount = 4
        pickerController.didSelectAssets = { (assets: [DKAsset]) in
            self.assets = assets
            self.imageCollection.reloadData()
        }
        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName : UIColor.blackColor()]
        UINavigationBar.appearance().tintColor = nil
        self.presentViewController(pickerController, animated: true) {}
    }
}