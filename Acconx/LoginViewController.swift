//
//  LoginViewController.swift
//  Acconx
//
//  Created by Tana Chaijamorn on 4/2/2559 BE.
//  Copyright © 2559 Tana Chaijamorn. All rights reserved.
//

import UIKit
import SwiftyJSON
import MBProgressHUD
import FBSDKCoreKit
import FBSDKLoginKit
import GoogleSignIn
import nanoUI

class LoginViewController: UIViewController,ConnectApiDelegate,FBSDKLoginButtonDelegate, GIDSignInDelegate, GIDSignInUIDelegate,UITextFieldDelegate {

    @IBOutlet weak var SignInLabel: UILabel!
    @IBOutlet weak var SignInBtn: UIButton!
    @IBOutlet weak var SignUpBtn: UIButton!
    @IBOutlet weak var ForgotPasswordBtn: UIButton!
    @IBOutlet weak var facebookBtn: FBSDKLoginButton!
    @IBOutlet weak var signInButton: GIDSignInButton!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet var menuButton:UIBarButtonItem!
    
    var push : Bool = false;
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.setNavigationbarToTranspalent()
//        if self.revealViewController() != nil {
//            menuButton.target = self.revealViewController()
//            menuButton.action = "revealToggle:"
//        }
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().uiDelegate = self
        
        self.SignInLabel.text = NSLocalizedString("signIn", comment: "signIn")
        self.emailField.placeholder = NSLocalizedString("email", comment: "email")
        self.passwordField.placeholder = NSLocalizedString("password", comment: "password")
        
//        self.SignInBtn .setTitle(NSLocalizedString("signIn", comment: "signIn"), forState: UIControlState.Normal)
        
        let SignUpBtnTitle = NSMutableAttributedString(string: NSLocalizedString("signUp", comment: "signUp"))
        
        SignUpBtnTitle.addAttributes([NSForegroundColorAttributeName : UIColor.whiteColor()], range: NSMakeRange(0, SignUpBtnTitle.length))
        SignUpBtnTitle.addAttributes([NSUnderlineStyleAttributeName : NSUnderlineStyle.StyleSingle.rawValue], range: NSMakeRange(0, SignUpBtnTitle.length))
        
        let ForgotPasswordBtnTitle = NSMutableAttributedString(string: NSLocalizedString("forgetPassword", comment: "forgetPassword"))
        ForgotPasswordBtnTitle.addAttributes([NSForegroundColorAttributeName : UIColor.whiteColor()], range: NSMakeRange(0, ForgotPasswordBtnTitle.length))
        ForgotPasswordBtnTitle.addAttributes([NSUnderlineStyleAttributeName : NSUnderlineStyle.StyleSingle.rawValue], range: NSMakeRange(0, ForgotPasswordBtnTitle.length))
        
        self.SignUpBtn.setAttributedTitle(SignUpBtnTitle, forState: UIControlState.Normal)
        self.ForgotPasswordBtn.setAttributedTitle(ForgotPasswordBtnTitle, forState: UIControlState.Normal)
        
        if (FBSDKAccessToken.currentAccessToken() != nil)
        {
            // User is already logged in, do work such as go to next view controller.
            self.facebookBtn.readPermissions = ["public_profile", "email", "user_friends"]
            self.facebookBtn.delegate = self
//            self.returnUserData()
        }
        else
        {
            self.facebookBtn.readPermissions = ["public_profile", "email", "user_friends"]
            self.facebookBtn.delegate = self
        }
        
        if Reachability.isConnectedToNetwork() == true {
            let api = ConnectApi()
            api.delegate = self
            var token = NSUserDefaults.standardUserDefaults().valueForKey("UserToken") as? String
            
            if (token == nil) {
                token = ""
            }
            
            let hud = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
            hud.mode = MBProgressHUDMode.Indeterminate
            hud.labelText = "Loading"
            api.checkAPI(token!) { responseObject, error in
                
                if responseObject!["message"] as? String == "Authorization has been denied for this request." {
                    print("can't acress")
                    hud.hide(true)
                }else{
                    
                    hud.hide(true)
                    self.pushtoHomewithAnimation(true)
                }
                
            }
        } else {
            let alertNetwork = UIAlertView(title: "No Internet Connection", message: "Make sure your device is connected to the internet.", delegate: nil, cancelButtonTitle: "OK")
            alertNetwork.show()
        }
        
        
        
    }
    
//    override func viewDidAppear(animated: Bool) {
//        super.viewDidAppear(true)
//        (self.view as! nUIView).showActivityIndicator()
//    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - IBAction
    @IBAction func SignInFunc(sender: AnyObject) {
        self.emailField.resignFirstResponder()
        self.passwordField.resignFirstResponder()
        if isValidEmail(self.emailField.text!) {
            
            let hud = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
            hud.mode = MBProgressHUDMode.Indeterminate
            hud.labelText = "Loading"
            
            let api = ConnectApi()
            api.delegate = self
            api.ApiLogin(self.emailField.text!, password: self.passwordField.text!)
            
        }else{
            self.setAlert("No Email")
        }
    }
    
    @IBAction func SignUpFunc(sender: AnyObject) {
    }
    
    @IBAction func ForgetPasswordFunc(sender: AnyObject) {
    }
    
    @IBAction func facebookLoginfunc(sender: AnyObject) {
        let login = FBSDKLoginManager.init()
        let permission = ["public_profile", "email", "user_friends"]
        
        login.logInWithReadPermissions(permission, fromViewController: self) { (result, error) in
            if ((error) != nil) {
                print("Process error")
            } else if (result.isCancelled) {
                print("Cancelled")
            } else {
                print("Log in")
                let graphRequest : FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "me", parameters: ["fields":"email,name"])
                graphRequest.startWithCompletionHandler({ (connection, result, error) -> Void in
                    
                    if ((error) != nil)
                    {
                        // Process error
                        print("Error: \(error)")
                    }
                    else
                    {
                        //                print("fetched user: \(result)")
                        let hud = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
                        hud.mode = MBProgressHUDMode.Indeterminate
                        hud.labelText = "Loading"
                        //                print(FBSDKAccessToken.currentAccessToken().tokenString)
                        let userName : NSString = result.valueForKey("name") as! NSString
                        //                print("User Name is: \(userName)")
                        
                        let userEmail : NSString = result.valueForKey("email") as! NSString
                        //                print("User Email is: \(userEmail)")
                        let api = ConnectApi()
                        
                        api.extranalLogin("Facebook", AccessToken: FBSDKAccessToken.currentAccessToken().tokenString, email: userEmail as String, completionHandler: { responseObject, error in
                            MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
                            let token_type = responseObject!["token_type"] as! String
                            let access_token =  responseObject!["access_token"] as! String
                            
                            let User = "\(token_type) \(access_token)"
                            
                            NSUserDefaults.standardUserDefaults().setObject(User, forKey: "UserToken")
                            self.pushtoHomewithAnimation(true)
                        })
                    }
                    
                })
            }
        }
    }
    
    @IBAction func googleLoginfunc(sender: AnyObject) {
        GIDSignIn.sharedInstance().signIn()
    }

    // MARK: - Validate
    func isValidEmail(testStr:String) -> Bool {
        // println("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluateWithObject(testStr)
    }

    func isValidPassword(testStr2:String) -> Bool {
        
        let passwordRegEx = "[A-Z0-9a-z._%+-:/><#]{6,30}"
        
        let passwordTest = NSPredicate(format: "SELF MATCHES %@", passwordRegEx)
        
        return passwordTest.evaluateWithObject(testStr2)
    }
    
    // MARK: - Set UI
    func setNavigationbarToTranspalent() {
        self.navigationController?.navigationBarHidden = false
        
        let bar:UINavigationBar! =  self.navigationController?.navigationBar
        
        if (bar == nil) {
            
        }else{
            bar.setBackgroundImage(UIImage(), forBarMetrics: UIBarMetrics.Default)
            bar.shadowImage = UIImage()
            bar.backgroundColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.0)
        }
        
    }
    
    func setAlert(AlertStr:String){
        let alertController = UIAlertController(title: "Acconx", message:
            AlertStr, preferredStyle: UIAlertControllerStyle.Alert)
        alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default,handler: nil))
        
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
    
    func getReceiveData(data: AnyObject, name: String) {
        dispatch_async(dispatch_get_main_queue(), {
            if data as! Int == 200 {
                MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
                self.pushtoHomewithAnimation(true)
            }else{
                MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
                self.setAlert("Can't login")
            }
        })
        
    }
    
    func pushtoHomewithAnimation(animated: Bool) {
        
        if self.revealViewController() != nil {
            print("have reveal")
            push = true
            let homeViewControllerObj = self.storyboard?.instantiateViewControllerWithIdentifier("HomeView") as? HomeViewController
            let navigation = UINavigationController.init(rootViewController: homeViewControllerObj!)
//            self.navigationController?.pushViewController(homeViewControllerObj!, animated: animated)
            self.revealViewController().pushFrontViewController(navigation, animated: animated)
        }
        
        
    }
    
    
    func loginButton(loginButton: FBSDKLoginButton!, didCompleteWithResult result: FBSDKLoginManagerLoginResult!, error: NSError!) {
        print("User Logged In")
        
        if ((error) != nil)
        {
            // Process error
        }
        else if result.isCancelled {
            // Handle cancellations
        }
        else {
            // If you ask for multiple permissions at once, you
            // should check if specific permissions missing
            if result.grantedPermissions.contains("email")
            {
                // Do work
                self.returnUserData()
            }
        }
    }
    
    func loginButtonDidLogOut(loginButton: FBSDKLoginButton!) {
        print("User Logged Out")
    }
    
    func returnUserData()
    {
        let graphRequest : FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "me", parameters: ["fields":"email,name"])
        graphRequest.startWithCompletionHandler({ (connection, result, error) -> Void in
            
            if ((error) != nil)
            {
                // Process error
                print("Error: \(error)")
            }
            else
            {
//                print("fetched user: \(result)")
                let hud = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
                hud.mode = MBProgressHUDMode.Indeterminate
                hud.labelText = "Loading"
//                print(FBSDKAccessToken.currentAccessToken().tokenString)
                let userName : NSString = result.valueForKey("name") as! NSString
//                print("User Name is: \(userName)")
                
                let userEmail : NSString = result.valueForKey("email") as! NSString
//                print("User Email is: \(userEmail)")
                let api = ConnectApi()
                
                api.extranalLogin("Facebook", AccessToken: FBSDKAccessToken.currentAccessToken().tokenString, email: userEmail as String, completionHandler: { responseObject, error in
                    MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
                    let token_type = responseObject!["token_type"] as! String
                    let access_token =  responseObject!["access_token"] as! String
                    
                    let User = "\(token_type) \(access_token)"
                    
                    NSUserDefaults.standardUserDefaults().setObject(User, forKey: "UserToken")
                    self.pushtoHomewithAnimation(true)
                })
            }
            
        })
    }
    
    // Implement these methods only if the GIDSignInUIDelegate is not a subclass of
    // UIViewController.
    
    // Stop the UIActivityIndicatorView animation that was started when the user
    // pressed the Sign In button
    func signInWillDispatch(signIn: GIDSignIn!, error: NSError!) {
//        myActivityIndicator.stopAnimating()
    }
    
    // Present a view that prompts the user to sign in with Google
    func signIn(signIn: GIDSignIn!,
                presentViewController viewController: UIViewController!) {
        self.presentViewController(viewController, animated: true, completion: nil)
    }
    
    // Dismiss the "Sign in with Google" view
    func signIn(signIn: GIDSignIn!,
                dismissViewController viewController: UIViewController!) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func signIn(signIn: GIDSignIn!, didSignInForUser user: GIDGoogleUser!, withError error: NSError!) {
        if (error == nil) {
            // Perform any operations on signed in user here.
            //            let userId = user.userID                  // For client-side use only!
//            let idToken : String = user.authentication.idToken // Safe to send to the server
            let idToken : String = GIDSignIn.sharedInstance().currentUser.authentication.accessToken
            let email : String = user.profile.email
            
            let api = ConnectApi()
            
            api.extranalLogin("Google", AccessToken: idToken, email: email, completionHandler: { responseObject, error in
                
                let token_type = responseObject!["token_type"] as! String
                let access_token =  responseObject!["access_token"] as! String
                let User = "\(token_type) \(access_token)"
                
                NSUserDefaults.standardUserDefaults().setObject(User, forKey: "UserToken")
                self.pushtoHomewithAnimation(true)
            })
        } else {
            print("\(error.localizedDescription)")
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if push == true {
            
        }
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        self.emailField.resignFirstResponder()
        self.passwordField.resignFirstResponder()
        return true
    }
}