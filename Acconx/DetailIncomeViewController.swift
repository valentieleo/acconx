//
//  DetailViewController.swift
//  Acconx
//
//  Created by Tana Chaijamorn on 4/27/2559 BE.
//  Copyright © 2559 Tana Chaijamorn. All rights reserved.
//

import UIKit
import CCBottomRefreshControl
import MBProgressHUD

class DetailIncomeViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    var dataList : NSMutableArray = []
    var pages : Int = 1
    let api = ConnectApi()
    let user : String = (NSUserDefaults.standardUserDefaults().valueForKey("UserToken") as? String)!
    
    @IBOutlet var menuButton:UIBarButtonItem!
    @IBOutlet weak var tableincome: UITableView!
    
    var refreshControl: UIRefreshControl!
    
    var typelist:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        dataList = NSMutableArray()
        let hud = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        hud.mode = MBProgressHUDMode.Indeterminate
        hud.labelText = "Loading"
        
        self.navigationItem.setRightBarButtonItem(menuButton, animated: true)
        //        self.navigationItem.setHidesBackButton(true, animated:true);
        
        
        // Do any additional setup after loading the view.
        self.tableincome.estimatedRowHeight = 68
        self.tableincome.rowHeight = UITableViewAutomaticDimension
        self.tableincome.contentInset = UIEdgeInsetsMake(-64, 0, 0, 0)
        
        if typelist! == "Income" {
            self.title = NSLocalizedString("incomeTypeIncome", comment: "incomeTypeIncome")
        }else if typelist! == "Estimate" {
            self.title = NSLocalizedString("estimateTypeEstimate", comment: "estimateTypeEstimate")
        }else if typelist! == "Expenses" {
            self.title = NSLocalizedString("accCategoryExpenses", comment: "accCategoryExpenses")
        }else if typelist! == "Clients" {
            self.title = NSLocalizedString("clients", comment: "clients")
        }else if typelist! == "Items" {
            self.title = NSLocalizedString("items", comment: "items")
        }else{
            self.title = NSLocalizedString("incomeTypeIncome", comment: "incomeTypeIncome")
        }
        
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
            let type :String = self.typelist!
            self.api.APIloadData(type, user: self.user, page: self.pages, completionHandler: { responseObject, error in
//                                print("\(responseObject) \n  \(error) ")
                
                self.dataList.addObjectsFromArray((responseObject?.objectForKey("collection") as? NSArray)! as [AnyObject])
                
//                if self.typelist! == "Income" {
//                    self.dataList.sortedArrayUsingDescriptors([NSSortDescriptor(key: "incomeDate", ascending: false)])
//                } else if self.typelist! == "Estimate" {
//                    self.dataList.sortedArrayUsingDescriptors([NSSortDescriptor(key: "estimateDate", ascending: false)])
//                } else if self.typelist! == "Expenses" {
//                    self.dataList.sortedArrayUsingDescriptors([NSSortDescriptor(key: "expenseDate", ascending: false)])
//                } else if self.typelist! == "Items" {
//                    self.dataList.sortedArrayUsingDescriptors([NSSortDescriptor(key: "name", ascending: true)])
//                }
                
                hud.hide(true)
                self.tableincome .reloadData()
            })
        }
        
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(DetailIncomeViewController.refresh(_:)), forControlEvents: UIControlEvents.ValueChanged)
        
        tableincome.bottomRefreshControl = refreshControl
    }
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(true)
        refreshControl.removeFromSuperview()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataList.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        if typelist! == "Income" {
            let cell = tableView.dequeueReusableCellWithIdentifier("IncomeCell", forIndexPath: indexPath) as! IncomeTableViewCell
            if (indexPath.row % 2) != 0 {
                cell.backgroundColor = UIColor.init(hex: "F7F7F7")
            }else{
                cell.backgroundColor = UIColor.whiteColor()
            }
            cell.ActivityName.text = dataList[indexPath.row]["clientContactName"] as? String
            cell.statusvalue.text = dataList[indexPath.row]["incomeStatus"] as? String
            cell.dateIncome.text = dataList[indexPath.row]["incomeDate"] as? String
            cell.Costvalue.text = dataList[indexPath.row]["totalAmount"] as? String
            cell.selectionStyle = .None
            
            return cell
        }else if typelist! == "Estimate" {
            let cell = tableView.dequeueReusableCellWithIdentifier("IncomeCell", forIndexPath: indexPath) as! IncomeTableViewCell
            if (indexPath.row % 2) != 0 {
                cell.backgroundColor = UIColor.init(hex: "F7F7F7")
            }else{
                cell.backgroundColor = UIColor.whiteColor()
            }
            
            cell.ActivityName.text = dataList[indexPath.row]["clientContactName"] as? String
            cell.statusvalue.text = dataList[indexPath.row]["estimateStatus"] as? String
            cell.dateIncome.text = dataList[indexPath.row]["estimateDate"] as? String
            cell.Costvalue.text = dataList[indexPath.row]["totalAmount"] as? String
            
            cell.selectionStyle = .None
            
            return cell
        }else if typelist! == "Expenses" {
            let cell = tableView.dequeueReusableCellWithIdentifier("IncomeCell", forIndexPath: indexPath) as! IncomeTableViewCell
            if (indexPath.row % 2) != 0 {
                cell.backgroundColor = UIColor.init(hex: "F7F7F7")
            }else{
                cell.backgroundColor = UIColor.whiteColor()
            }
            cell.selectionStyle = .None
            cell.statusvalue?.text = ""
            
            cell.ActivityName.text = dataList[indexPath.row]["vendorName"] as? String
            cell.dateIncome.text = dataList[indexPath.row]["expenseDate"] as? String
            cell.Costvalue.text = dataList[indexPath.row]["amount"] as? String
            
            return cell
        }else if typelist! == "Items" {
            let cell = tableView.dequeueReusableCellWithIdentifier("ClientAndItemsCell", forIndexPath: indexPath) as! ClientAndItemsCell
            if (indexPath.row % 2) != 0 {
                cell.backgroundColor = UIColor.init(hex: "F7F7F7")
            }else{
                cell.backgroundColor = UIColor.whiteColor()
            }
            
            cell.selectionStyle = .None
            cell.itemLabel.text = dataList[indexPath.row]["name"] as? String
            
            print(dataList.objectAtIndex(indexPath.row).objectForKey("sellPrice"))
            
            //            cell.priceLabel.text = dataList.objectAtIndex(indexPath.row).objectForKey("sellPrice") as? String
            //if dataList[indexPath.row]["sellPrice"]{
                    // }
            //            var number : Float = ((dataList[indexPath.row]["sellPrice"] as? NSNumber)?.floatValue)!
            if dataList[indexPath.row]["sellPrice"] as? NSNull ==  NSNull() {
                cell.priceLabel.text = ""
            }else{
                let price = dataList[indexPath.row]["sellPrice"] as! NSNumber
                cell.priceLabel.text = "\(price)"
            }
            
            
            return cell
        }else{
            let cell = tableView.dequeueReusableCellWithIdentifier("IncomeCell", forIndexPath: indexPath) as! IncomeTableViewCell
            if (indexPath.row % 2) != 0 {
                cell.backgroundColor = UIColor.init(hex: "F7F7F7")
            }else{
                cell.backgroundColor = UIColor.whiteColor()
            }
            
            cell.selectionStyle = .None
            
            return cell
        }
        
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if typelist! == "Income" {
            let DetailVC = self.storyboard?.instantiateViewControllerWithIdentifier("DetailInfoView") as? DetailInfoViewController
            DetailVC?.typelist = "Income"
            DetailVC?.dataList = dataList[indexPath.row] as! NSDictionary
            self.navigationController?.pushViewController(DetailVC!, animated: true)
        }else if typelist! == "Estimate" {
            let DetailVC = self.storyboard?.instantiateViewControllerWithIdentifier("DetailInfoView") as? DetailInfoViewController
            DetailVC?.typelist = "Estimate"
            DetailVC?.dataList = dataList[indexPath.row] as! NSDictionary
            self.navigationController?.pushViewController(DetailVC!, animated: true)
        }
//        else if typelist! == "Expenses" {
//            let DetailVC = self.storyboard?.instantiateViewControllerWithIdentifier("DetailInfoView") as? DetailInfoViewController
//            DetailVC?.typelist = "Expenses"
//            DetailVC?.dataList = dataList[indexPath.row] as! NSDictionary
//            self.navigationController?.pushViewController(DetailVC!, animated: true)
//        }else{
//            
//        }
    }
    
    
    func refresh(sender:AnyObject) {
        // Code to refresh table view
        pages += 1
        
        let hud = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        hud.mode = MBProgressHUDMode.Indeterminate
        hud.labelText = "Loading"
        
        self.api.APIloadData("income", user: self.user, page: self.pages, completionHandler: { responseObject, error in
            //            print("\(responseObject) \n  \(error) ")
            
            self.dataList.addObjectsFromArray((responseObject?["collection"] as? NSArray)! as [AnyObject])
            
            hud.hide(true)
            self.tableincome .reloadData()
        })
        
        refreshControl.endRefreshing()
    }
    
    @IBAction func AdditemFunc(sender: AnyObject) {
        
    }
    
    
}
